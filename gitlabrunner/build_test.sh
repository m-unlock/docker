#!/bin/bash
#============================================================================
#title          :Gitlab runner base image
#description    :Image to be used for the gitlab runner
#author         :Jasper Koehorst
#date           :2022
#version        :0.0.1
#============================================================================

docker login git@docker-registry.wur.nl  

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C $DIR pull

#============================================================================
# Build the docker file
#============================================================================
cd $DIR

docker pull docker-registry.wur.nl/m-unlock/docker/gitlab:runner

docker build -t docker-registry.wur.nl/m-unlock/docker/gitlab:runner .

docker push docker-registry.wur.nl/m-unlock/docker/gitlab:runner