export SSHPASS=$PASSWORD
sshpass -e sftp -o StrictHostKeyChecking=no -oBatchMode=no -b - $USERNAME@$HOST << !
   cd /upload/unlock
   put $1
   bye
!