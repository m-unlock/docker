export SSHPASS=$PASSWORD
sshpass -e sftp -o StrictHostKeyChecking=no -oBatchMode=no -b - $USERNAME@$HOST << !
   cd $2
   put $1
   bye
!