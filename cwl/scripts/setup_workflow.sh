#!/bin/bash

#Setup CWL directory

# $1 = Workflow name. i.e workflow_nanopore_assembly

workflow=$(basename $1 .cwl)

mkdir -p /unlock/infrastructure/cwl
git clone https://gitlab.com/m-unlock/cwl.git /cwl/setup.tmp

# Move neccesary files to permanent dependency location
cp -r /cwl/setup.tmp/cwl /cwl/setup.tmp/setup /unlock/infrastructure/

# Execute Workflow dependency setup script:
if [ ! -f /unlock/infrastructure/setup/setup_$workflow.sh ]; then
    echo "#############################################"
    echo "$1 setup file not found!"
    echo "The following setup files are available:"
    ls /unlock/infrastructure/setup/ | sed 's/setup_//' | sed 's/.sh$//'
    echo "#############################################"
    exit 0
fi

bash /unlock/infrastructure/setup/setup_$1.sh