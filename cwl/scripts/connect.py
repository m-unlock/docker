from irods.session import iRODSSession
import ssl
from irods.meta import iRODSMeta
import os, sys

# iRODS authentication information
host = os.getenv('irodsHost')
port = os.getenv('irodsPort')
zone = os.getenv('irodsZone')
user = os.getenv('irodsUserName')
password = os.getenv('irodsPassword')

print("Connecting to iRODS server: ", host, port, zone, user, password)

# SSL settings
context = ssl.create_default_context(purpose=ssl.Purpose.SERVER_AUTH, cafile=None, capath=None, cadata=None)

ssl_settings = {
    'irods_client_server_negotiation': 'request_server_negotiation',
    'irods_client_server_policy': 'CS_NEG_REQUIRE',
    'irods_encryption_algorithm': 'AES-256-CBC',
    'irods_encryption_key_size': 32,
    'irods_encryption_num_hash_rounds': 16,
    'irods_encryption_salt_size': 8,
    'ssl_context': context
    }

# Authentication
with iRODSSession(
    host = host,
    port = port,
    user = user,
    password = password,
    zone = zone,
    **ssl_settings) as session:

    print("Connected to iRODS ", session.server_version)
