from itertools import count
import os, re
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-clustalo", help="Path to clustalo binary", required=True)
parser.add_argument("-fasta", help="Fasta input file", required=True)
parser.add_argument("-output", help="Clustalo output file", required=True)
parser.add_argument("-gtree", help="Clustalo guide tree output file", required=True)
parser.add_argument("-distmat", help="Clustalo distance matrix file", required=True)
parser.add_argument("-threads", help="Number of threads to use", required=True)

args = parser.parse_args()

def main(args):
    lookup = fix_headers(args.fasta)

    clustalo = args.clustalo
    input_file = "--in=" + args.fasta + ".fasta"
    distmat = "--distmat-out=" + args.distmat
    threads = "--threads=" + str(args.threads)
    arguments = "--full -v --force"
    outfile = "--outfile=" + args.output
    treefile = "--guidetree-out=" + args.gtree
    command = clustalo + " " + input_file + " " + distmat + " " + threads + " " +arguments + " " + outfile + " " + treefile
    os.system(command)

    fix_output(args.distmat, lookup)
    fix_output(args.output, lookup)
    fix_output(args.gtree, lookup)
    # TODO GZIP DIST

def fix_headers(input_file):
    output = open(input_file + ".fasta", "w")
    lookup = {}
    counter = 0
    for line in open(input_file):
        if line.startswith(">"):
            header = line.strip(">").strip()
            counter = counter + 1
            new_header = "HEADER_" + str(counter) + "_HEADER"
            lookup[new_header] = header
            line = ">" + new_header
        print(line, file=output)
    output.close()
    return lookup

def fix_output(input_file, lookup):
    print("Reading " + input_file)
    lines = open(input_file).readlines()
    output = open(input_file, "w")
    for line in lines:
        if "HEADER_" in line:
            matching = re.findall("HEADER_[0-9]+_HEADER", line)[0]
            # Added split for lines with >ID subfragment=... to only obtain the ID
            line = line.replace(matching, lookup[matching].split()[0])
        print(line.strip(), file=output)
    output.close()


if __name__ == '__main__':
    main(args)