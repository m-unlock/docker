#!/bin/bash

usage() { echo "Usage: $0 [-c <cwl file path>] [-y <yaml file path>] [-p <true|false>]" 1>&2; exit 1; }

while getopts ":c:y:p:d:" o; do
    case "${o}" in
        c)
            cwl=${OPTARG}
            ;;
        y)
            yaml=${OPTARG}
            ;;
        p)
            prov=${OPTARG}
            ((prov == true || prov == false)) || usage
            ;;
        d)
            debug=${OPTARG}
            ((debug == true || debug == false)) || usage
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${cwl}" ] || [ -z "${yaml}" ] || [ -z "${prov}" ]; then
    usage
fi

echo "c = ${cwl}"
echo "y = ${yaml}"
echo "p = ${prov}"
echo "d = ${debug}"

echo $@

#######################################
# Set debug variable
DEBUG=""
if [ "$debug" = true ]
then  
  DEBUG="--debug"
fi

#######################################
# Activate virtual environment
. /opt/venv/bin/activate

#######################################
# iRODS connect test
./iinit_script.exp $irodsPassword

# python3.10 /scripts/connect.py
# [ $? -eq 0 ] && echo "iRODS connection succesfull" || exit 1

yaml_filepath=$yaml
# Obtain parent directory
yaml_dir="$(dirname "$yaml")"/
# Obtain the yaml name removing all spaces for security purposes (rm ...)
yaml_file_name="$(basename "$yaml")"
yaml_file_name="$(echo -e "${yaml_file_name}" | tr -d '[:space:]')"
echo "YAML FILE NAME:"$yaml_file_name

# Download the yaml file $1=yaml full path
mkdir -p $yaml_dir
# Delete yaml file if already exists
rm -f $yaml

# Updating metadata by removing all cwl and cwl path status
# python3.10 /scripts/metadata.py -key cwl -value $cwl -remove -file $yaml
# [ $? -eq 0 ] && echo "Metadata removed" || exit 1

# Adding the running status
# python3.10 /scripts/metadata.py -key cwl -value $cwl -unit running -add -file $yaml
# [ $? -eq 0 ] && echo "Metadata added" || exit 1

# Download latest yaml file
mkdir -p $(dirname "$yaml")
iget -P $yaml $yaml
# /root/.sdkman/candidates/java/current/bin/java -jar /usr/bin/IRODSTransfer.jar --force --threads 4 -pull --files $yaml
[ $? -eq 0 ] && echo "downloaded $yaml" || exit 1

# Obtain destination path
destination_path=`cat $yaml | grep destination | awk -F": " '{print $2}'`

# Download run files from iRODS
# SET _JAVA_OPTIONS = -Xms5120m -Xmx10240m

# Download all excluding reference folder
paths=$(grep "[0-9]\+_irods:" "$yaml" | awk '{print $2}' | sort | uniq)
# Loop through the IDs
for file_path in $paths; do
    # Download the file
    echo "Downloading $file_path"
    mkdir -p $(dirname "$file_path")
    iget -P $file_path $file_path
done

# Download all excluding reference folder
# grep "[0-9]\+_irods:" $yaml | grep -v "/references/" | awk '{print $2}' | sort | uniq | tr '\n' ' ' | awk '{print "/root/.sdkman/candidates/java/current/bin/java -jar /usr/bin/IRODSTransfer.jar --threads 4 --pull --files "$0}' | bash -x

# Download only reference folder
# grep "[0-9]\+_irods:" $yaml | grep "/references/" | awk '{print $2}' | sort | uniq | tr '\n' ' ' | awk '{print "/root/.sdkman/candidates/java/current/bin/java -jar /usr/bin/IRODSTransfer.jar --threads 4 --pull --files "$0}'
# grep "[0-9]\+_irods:" $yaml | grep "/references/" | awk '{print $2}' | sort | uniq | xargs -I'{}' /root/.sdkman/candidates/java/current/bin/java -jar /usr/bin/IRODSTransfer.jar --threads 4 --pull --files {}

# Check exit status of cwl
exit_status=$?

if [ $exit_status -eq 0 ]; then
    echo "Finished download files"
else
    echo "Download failed"
    exit 1
fi

# Run workflow with or without provenance
if $prov; then
    cwltool $DEBUG --cachedir $destination_path/CACHE_$yaml_file_name --tmpdir-prefix /unlock/rancher/unlockcwl --outdir $destination_path --no-data --provenance $destination_path/PROVENANCE $cwl $yaml
else
    cwltool $DEBUG --cachedir $destination_path/CACHE_$yaml_file_name --tmpdir-prefix /unlock/rancher/unlockcwl --outdir $destination_path $cwl $yaml
fi

# Check exit status of cwl
exit_status=$?

# Make sure conda is deactivated afterwards in addition cwl scripts should do this on their own
# source /root/miniconda/bin/activate && conda deactivate

# When success, update avu on yaml file and upload results
if [ $exit_status -eq 0 ]; then
    # Remove provenance data file
    if $prov; then
        # Removes the data folder with empty files
        rm -r $destination_path/PROVENANCE/data/

        # Copy TTL files from provenance folder
        find $destination_path/PROVENANCE -type f | grep "ttl$" | xargs cat | rapper -i turtle -o turtle - http://baseuri > $destination_path/temp_provenance.ttl

        # Rehash blank nodes
        rdfhash --data $destination_path/temp_provenance.ttl > $destination_path/provenance.ttl
        rm $destination_path/temp_provenance.ttl

        # Compress provenance folder
        tar czf $destination_path/PROVENANCE.tar.gz -C $destination_path PROVENANCE

        # Remove docker folder
        rm -rf $destination_path/PROVENANCE
    fi

    # Remove cache data
    echo removing $destination_path/CACHE_$yaml_file_name*
    rm -r $destination_path/CACHE_$yaml_file_name*
    
    # Upload the provenance
    # /root/.sdkman/candidates/java/current/bin/java -jar /usr/bin/IRODSTransfer.jar --threads 4 --push --files $destination_path --irods $destination_path
    echo irsync -rV $destination_path/ i:$destination_path
    irsync -rV $destination_path/ i:$destination_path

    
    # Update metadata field
    # python3.10 /scripts/metadata.py -key cwl -value $cwl -remove -file $yaml
    # python3.10 /scripts/metadata.py -key cwl -value $cwl -unit finished -add -file $yaml

    # Remove local destination results
    rm -rf $destination_path

else
    # When CWL execution fails update metadata
    echo "CWL execution failed..."
    # python3.10 /scripts/metadata.py -key cwl -value $cwl -remove -file $yaml
    # python3.10 /scripts/metadata.py -key cwl -value $cwl -unit failed -add -file $yaml
    exit 1
fi

# Remove downloaded files
echo "Removing unprocessed irods transfered files"
grep "[0-9]\+_irods: /unlock/projects/.*/unprocessed" $yaml | awk '{print "rm -f "$2}' | sh

exit 0
