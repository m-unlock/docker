#!/bin/bash

usage() { echo "Usage: $0 [-c <cwl file path>] [-y <yaml file path>] [-p <true|false>]" 1>&2; exit 1; }

while getopts ":c:y:p:" o; do
    case "${o}" in
        c)
            cwl=${OPTARG}
            ;;
        y)
            yaml=${OPTARG}
            ;;
        p)
            prov=${OPTARG}
            ((prov == true || prov == false)) || usage
            ;;
        d)
            debug=${OPTARG}
            ((debug == true || debug == false)) || usage
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${cwl}" ] || [ -z "${yaml}" ] || [ -z "${prov}" ]; then
    usage
fi

echo "c = ${cwl}"
echo "y = ${yaml}"
echo "p = ${prov}"
echo "d = ${debug}"

echo $@

#######################################
# Set debug variable
DEBUG=""
if [ "$debug" = true ]
then  
  DEBUG="--debug"
fi


#######################################
# Activate virtual environment
. /opt/venv/bin/activate

# Obtain the yaml name
yaml_file_name="$(basename "$yaml")"

# Obtain destination path
destination_path=`cat $yaml | grep destination | awk -F": " '{print $2}'`

if [ -z "$destination_path" ];
then
    echo "Please specify a 'destination' in your yaml file"
    echo "For example: destination: /output/my_workflow_run"
    exit 1
fi

if [ ! -d /unlock/tmpdir ]; then
  mkdir -p /unlock/tmpdir;
fi

# Run workflow with or without provenance
if $prov; then
    cwltool $DEBUG --cachedir $destination_path/CACHE_$yaml_file_name --tmpdir-prefix /unlock/tmpdir/unlockcwl --no-container --preserve-entire-environment --outdir $destination_path --no-data --provenance $destination_path/PROVENANCE $cwl $yaml
else
    cwltool $DEBUG --cachedir $destination_path/CACHE_$yaml_file_name --tmpdir-prefix /unlock/tmpdir/unlockcwl --no-container --preserve-entire-environment --outdir $destination_path $cwl $yaml
fi

# Check exit status of cwl
exit_status=$?

# When success, update avu on yaml file and upload results
if [ $exit_status -eq 0 ]; then
    # Remove provenance data file
    if $prov; then
        # Removes the data folder with empty files
        rm -r $destination_path/PROVENANCE/data/

        # Copy TTL files from provenance folder
        find $destination_path/PROVENANCE -type f | grep "ttl$" | xargs cat | rapper -i turtle -o turtle - http://baseuri > $destination_path/temp_provenance.ttl

        # Rehash blank nodes
        rdfhash --data $destination_path/temp_provenance.ttl > $destination_path/provenance.ttl
        rm $destination_path/temp_provenance.ttl

        # Compress provenance folder
        tar czf $destination_path/PROVENANCE.tar.gz -C $destination_path PROVENANCE

        # Remove docker folder
        rm -rf $destination_path/PROVENANCE
    fi

    # Remove cache data
    echo removing $destination_path/CACHE_$yaml_file_name
    rm -r $destination_path/CACHE_$yaml_file_name
    
else
    # When CWL execution fails update metadata
    echo "#################################"
    echo "CWL execution failed..."
    echo "Execute following command for more information about the workflow:"
    echo cwltool $cwl --help
    echo "#################################"
    exit 1
fi