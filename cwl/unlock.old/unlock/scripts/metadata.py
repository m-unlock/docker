from irods.session import iRODSSession
import ssl
from irods.meta import iRODSMeta
import os, sys

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-key", help="Key of the irods metadata field", required=True)
parser.add_argument("-value", help="Value of the irods metadata field", required=True)
parser.add_argument("-unit", help="Unit of the irods metadata field")
parser.add_argument("-add", help="Key,Value,Unit to be added as metadata field", action="store_true")
parser.add_argument("-remove", help="Key,Value,Unit to be removed as metadata field", action="store_true")
parser.add_argument("-file", help="Path to the file on the irods system of which the metadata needs to be modified", required=True)

args = parser.parse_args()

if not args.add and not args.remove:
    print("Either -add or -remove is required")
    sys.exit(1)

# iRODS authentication information
host = os.getenv('irodsHost')
port = os.getenv('irodsPort')
zone = os.getenv('irodsZone')
user = os.getenv('irodsUserName')
password = os.getenv('irodsPassword')

# SSL settings
context = ssl.create_default_context(purpose=ssl.Purpose.SERVER_AUTH, cafile=None, capath=None, cadata=None)

ssl_settings = {'irods_client_server_negotiation': 'request_server_negotiation',
                'irods_client_server_policy': 'CS_NEG_REQUIRE',
                'irods_encryption_algorithm': 'AES-256-CBC',
                'irods_encryption_key_size': 32,
                'irods_encryption_num_hash_rounds': 16,
                'irods_encryption_salt_size': 8,
                'ssl_context': context}

# Authentication
with iRODSSession(
    host = host,
    port = port,
    user = user,
    password = password,
    zone = zone,
    **ssl_settings) as session:

    print("Connected to iRODS ", session.server_version)

    obj = session.data_objects.get(args.file)
    # Adding requires full triple
    if args.add:
        obj.metadata.add(args.key, args.value, args.unit)
    # Removing can be achieved with or without the unit value
    if args.remove:
        print("Removing...", args.key, args.value, args.unit)
        if args.unit:
            obj.metadata.remove(args.key, args.value, args.unit)
        else:
            # Remove metadata without the unit variable
            for avu in obj.metadata.items():
                if avu.name == args.key and avu.value == args.value:
                    print("Removing avu by partial matching of", args.key, args.value)
                    obj.metadata.remove(avu)
