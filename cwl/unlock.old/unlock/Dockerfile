# Base Image
FROM docker-registry.wur.nl/m-unlock/docker/cwl:latest

# Metadata
LABEL base.image="docker-registry.wur.nl/unlock/docker"
LABEL version="1"
LABEL software="BASE 1.0"
LABEL software.version="0.0.0"
LABEL description="CWL base image for unlock internal use"
LABEL website="https://m-unlock.gitlab.io"
LABEL documentation="NA"
LABEL license="NA"
LABEL tags="Base"

ARG R_VERSION=4.1.1
ARG OS_IDENTIFIER=ubuntu-2204
# ARG cwltool_version=3.1.20211004060744

ENV DEBIAN_FRONTEND=noninteractive LANG=en_US.UTF-8 LC_ALL=C.UTF-8 LANGUAGE=en_US.UTF-8
ENV SDKMAN=/root/.sdkman/candidates/maven/current/bin:/root/.sdkman/candidates/java/current/bin:/root/.sdkman/candidates/gradle/current/bin

# Link shell to bash
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# Enable icommands
RUN apt-get update && \
	wget -qO - https://packages.irods.org/irods-signing-key.asc | apt-key add - && \
	echo "# /etc/apt/sources.list.d/renci-irods.list" | tee -a /etc/apt/sources.list.d/renci-irods.list && \
	echo "deb [arch=amd64] https://packages.irods.org/apt/ bionic main" | tee -a /etc/apt/sources.list.d/renci-irods.list && \
	apt-get update && apt-get install --no-install-recommends --yes irods-icommands expect

# Java installation
RUN curl -s https://get.sdkman.io | bash && \
	chmod a+x "$HOME/.sdkman/bin/sdkman-init.sh" && \
	echo "sdkman_auto_complete=false" >> "$HOME/.sdkman/etc/config" && \
	source "$HOME/.sdkman/bin/sdkman-init.sh" &&\
	sdk install java 17.0.1-tem &&\
	rm  -rf /root/.sdkman/archives

# Get some binaries, to be replaced with iCommands
RUN curl http://download.systemsbiology.nl/unlock/IRODSTransfer.jar -o /usr/bin/IRODSTransfer.jar

# Install tree and expect
RUN apt-get update && \
	apt-get install -y tree expect && \
	apt-get autoremove

# Add the upload script and install sshpass
COPY /scripts/upload.sh upload.sh

# Scripts for small management tasks
COPY scripts /scripts

# Add the startup scripts
COPY run.sh /usr/bin/run.sh

# Make it executable
RUN chmod +x /usr/bin/run.sh

# Create robot irods environment
RUN mkdir /root/.irods
COPY irods_environment.json /root/.irods/irods_environment.json
COPY iinit_script.exp /iinit_script.exp
RUN chmod +x /iinit_script.exp

# Activate default conda environment to ensure rdfhash and rapper are working
RUN echo ". /opt/venv/bin/activate" >> ~/.bashrc