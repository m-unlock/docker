#!/bin/bash
#============================================================================
#title          :build.sh
#description    :Script build and push docker image
#author         :Jasper Koehorst
#date           :2019
#version        :0.0.1
#============================================================================

docker login git@docker-registry.wur.nl  

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C $DIR pull

#============================================================================
# Build the docker file
#============================================================================

docker pull docker-registry.wur.nl/m-unlock/docker/cwl:2.0

docker build --platform linux/amd64 --build-arg branch=main -t docker-registry.wur.nl/m-unlock/docker/cwl:2.0 .
docker push docker-registry.wur.nl/m-unlock/docker/cwl:2.0

docker build --platform linux/amd64 --build-arg branch=main -t docker-registry.wur.nl/m-unlock/docker/cwl:latest .
docker push docker-registry.wur.nl/m-unlock/docker/cwl:latest
