#!/usr/bin/env bash

docker login git@docker-registry.wur.nl  

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ${DIR}

# To build the jupyter docker image
docker build -t docker-registry.wur.nl/unlock/docker:jupyter ./jupyter/

ret=$?

if [[ ${ret} -eq 0 ]]; then
    docker push docker-registry.wur.nl/unlock/docker:jupyter
else
    echo "Docker jupyter build failed stopping now "${ret}
    exit 1
fi

# To build the rstudio docker image
docker build -t docker-registry.wur.nl/unlock/docker:rstudio ./rstudio/

ret=$?

if [[ ${ret} -eq 0 ]]; then
    docker push docker-registry.wur.nl/unlock/docker:rstudio
else
    echo "Docker rstudio build failed stopping now"
    exit 1
fi

# To build the graphdb docker image
docker build -t docker-registry.wur.nl/unlock/docker:graphdb ./graphdb/

ret=$?

if [[ ${ret} -eq 0 ]]; then
    docker push docker-registry.wur.nl/unlock/docker:graphdb
else
    echo "Docker graphdb build failed stopping now"
    exit 1
fi