#!/bin/bash
#============================================================================
#title          :CWL dependencies installation script
#description    :Script to download all stuff needed for the dockers to work
#author         :Jasper Koehorst
#date           :2019
#version        :0.0.1
#============================================================================

docker login git@docker-registry.wur.nl  

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C $DIR pull

#============================================================================
# Build the docker file
#============================================================================

docker pull docker-registry.wur.nl/m-unlock/docker/irods

docker build -t docker-registry.wur.nl/m-unlock/docker/irods .

docker push docker-registry.wur.nl/m-unlock/docker/irods