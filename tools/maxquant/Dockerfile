# The build-stage image:
FROM continuumio/miniconda3 AS build

# Install mamba
RUN conda install -c conda-forge mamba 

# Install the package as normal:
COPY environment.yml .
RUN mamba env create -f environment.yml

# Install conda-pack:
RUN mamba install -c conda-forge conda-pack

# Use conda-pack to create a standalone enviornment
# in /venv:
RUN conda-pack -n start -o /tmp/env.tar && \
  mkdir /venv && cd /venv && tar xf /tmp/env.tar && \
  rm /tmp/env.tar

# We've put venv in same path it'll be in final image,
# so now fix up paths:
RUN /venv/bin/conda-unpack


# The runtime-stage image; we can use Debian as the
# base image since the Conda env also includes Python
# for us.
FROM debian:buster AS runtime

SHELL ["/bin/bash", "-c"]

# Copy /venv from the previous stage:
COPY --from=build /venv /venv
COPY start.sh /start.sh

# Need to obtain a copy of MaxQuant 2.4.0.0 (at the time of writing)
# Make sure to replace the space by _ in the file name
COPY MaxQuant_2.4.0.0 /MaxQuant_2.4.0.0

ENTRYPOINT ["bash", "/start.sh"] 