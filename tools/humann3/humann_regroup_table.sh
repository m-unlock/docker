#!/bin/bash

threads="$1"  # get threads
shift # remove it from the input arguments
custom_output_filename="$1"
shift
uniref_type="$1"
shift

args=("$@")
input_file=$(basename "${args[1]}" .tsv)
input_group="${args[-1]}"

groups=("rxn" "go" "ko" "level4ec" "pfam" "eggnog")

echo $input_group
echo $args

if [[ "$input_group" == "all" ]]; then
    unset args[$(( ${#args[@]} - 1 ))]  # Remove the last element (inputgroup/all)
    for group in "${groups[@]}"; do # Loop through all groups

        if [[ "$custom_output_filename" == "None" ]]; then
            output_filename=$input_file"_"$uniref_type"_"$group".tsv"
        else
            output_filename=$(basename "${custom_output_filename}" .tsv)"_"$group".tsv"
        fi
         # Fill regroup commands in a file to be executed in parallel
         echo "humann_regroup_table "${args[@]}" "$uniref_type"_"$group" --output "$output_filename >> all_group_runs
     done
    
     parallel -P $threads < all_group_runs # execute regroup commands in parallel

else # One specific group is given
    if [[ $custom_output_filename == "None" ]]; then
        output_filename=$input_file"_"$uniref_type"_"$input_group".tsv"
    else
        output_filename=$(basename "${custom_output_filename}" .tsv)"_"$input_group".tsv"
    fi
    
    humann_regroup_table ${args[@]} --output $output_filename # execute
fi