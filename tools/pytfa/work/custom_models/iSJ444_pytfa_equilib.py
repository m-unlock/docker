import os
import cobra
import pytfa
from cobra.io import load_json_model
from pytfa.io import import_matlab_model, load_thermoDB,                    \
                            read_lexicon, annotate_from_lexicon,            \
                            read_compartment_data, apply_compartment_data
from pytfa.io.json import save_json_model
from pytfa.thermo.equilibrator import build_thermo_from_equilibrator

# 1. Load the cobra_model and preprocess
#cobra_model = import_matlab_model("iSJ444.mat")
cobra_model = load_json_model('./models/iSJ444_annotated.json')

# Set the optimizer to Gurobi (GPLK or CPLEX)
cobra_model.solver = "cplex"

# 2. read in thermodynamic database
# thermo_data = load_thermoDB('thermo_data.thermodb')
# or Build the thermodynamics structure from eQuilibrator
thermo_data = build_thermo_from_equilibrator(cobra_model)

# 3. Read in the lexicon and compartment data
lexicon = read_lexicon('./models/lexicon.csv')
compartment_data = read_compartment_data('./models/compartment_data_ph_6_5.json')
annotate_from_lexicon(cobra_model, lexicon)

# Normalize annotation ids (but don't overwrite them)
for met in cobra_model.metabolites:
    met.annotation["seed.compound"] = met.annotation["seed_id"]

# 4. Initialize the ThermoModel with the thermodynamics data
tmodel = pytfa.ThermoModel(thermo_data, cobra_model)
tmodel.solver = "optlang-cplex"
tmodel.name = 'iSJ444'

# 5. Annotate the cobra_model
# annotate_from_lexicon(tmodel, lexicon)
apply_compartment_data(tmodel, compartment_data)

# Solver settings
def apply_solver_settings(model, solver = tmodel.solver):
    model.solver = solver
    # model.solver.configuration.verbosity = 1
    model.solver.configuration.tolerances.feasibility = 1e-9
    if solver == 'optlang_gurobi':
        model.solver.problem.Params.NumericFocus = 3
    model.solver.configuration.presolve = True

#apply_solver_settings(tmodel, tmodel.solver)


# 6. TFA conversion
# Prepare the thermodynamics model
tmodel.prepare()
tmodel.convert()

## Info on the cobra_model
tmodel.print_info()

# Optimize the model using Gurobi
solution = tmodel.optimize()
tfa_value = solution.objective_value

# Display some of the results
print("Some fluxes")
print(solution.fluxes.head())
print("\nObjective solution:", solution.objective_value)
print("\nTotal sum of fluxes:", solution.fluxes.sum())

# save the model
# save_json_model(tmodel, './models/iSJ444_tfa.json')