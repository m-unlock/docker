import os
import pdb
import copy
import cobra
import pytfa
from cobra.io import load_json_model
from pytfa.io import import_matlab_model, load_thermoDB,                    \
                            read_lexicon, annotate_from_lexicon,            \
                            read_compartment_data, apply_compartment_data
from pytfa.io.json import save_json_model
from pytfa.thermo.equilibrator import build_thermo_from_equilibrator

# 1. Load the cobra_model and preprocess
#cobra_model = import_matlab_model("iSJ444.mat")
cobra_model = load_json_model('./models/iSJ444_annotated.json')

# Set the optimizer to Gurobi (GPLK or CPLEX)
cobra_model.solver = "cplex"

# 2. Read in the lexicon and compartment data
lexicon = read_lexicon('./models/lexicon.csv')
compartment_data = read_compartment_data('./models/compartment_data_ph_6_5.json')
annotate_from_lexicon(cobra_model, lexicon)

# Normalize annotation ids (but don't overwrite them)
for met in cobra_model.metabolites:
    met.annotation["seed.compound"] = met.annotation["seed_id"]
    met.annotation["SeedID"] = met.annotation["seed_id"]
    
# 3. read in thermodynamic database
# thermo_data_default = load_thermoDB('thermo_data.thermodb')
# or Build the thermodynamics structure from eQuilibrator
thermo_data = build_thermo_from_equilibrator(cobra_model)

thermo_data_seedid = thermo_data.copy()
thermo_data_seedid['metabolites'] = {}
for met in thermo_data['metabolites']:
    for id_registry in met['other_names']:
        if id_registry.registry.namespace == 'seed':
            seed_id_temp = id_registry.accession
    if seed_id_temp:
        met['id'] = seed_id_temp
        thermo_data_seedid['metabolites'][seed_id_temp] = met
    else:
        print(f"No seed_id found!")
    # set defailt charge_std to 0 if none
    if met['charge_std'] is None or met['charge_std'] == 1000.0:
        met['charge_std'] = 1000
    # set error to Nil to inlude this metabolite
    if met['error'] is not 'Nil':
        if met['pKa']:
            met['error'] = 'Nil'

# 4. Initialize the ThermoModel with the thermodynamics data
tmodel = pytfa.ThermoModel(thermo_data_seedid, cobra_model)
tmodel.solver = "optlang-cplex"
tmodel.name = 'iSJ444'

# 5. Annotate the cobra_model
annotate_from_lexicon(tmodel, lexicon)
apply_compartment_data(tmodel, compartment_data)

# 6. TFA conversion
# Prepare the thermodynamics model
tmodel.prepare()
tmodel.convert()

## Info on the cobra_model
tmodel.print_info()

# Optimize the model using Gurobi
solution = tmodel.optimize()
tfa_value = solution.objective_value

# Display some of the results
print("Some fluxes")
print(solution.fluxes.head())
print("\nObjective solution:", solution.objective_value)
print("\nTotal sum of fluxes:", solution.fluxes.sum())