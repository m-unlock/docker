bokeh>=0.12.1
cobra==0.16.0          # Fixed version
equilibrator-cache==0.2.6  # Fixed version, 2020/01/06
equilibrator-api==0.2.6   # Fixed version, 2020/01/06
numpy==1.18.5          # Released in May 2020
ipdb==0.13.3           # Released in May 2020
lxml==4.5.2            # Released in July 2020
networkx==2.4          # Released in December 2019
openpyxl==3.0.4        # Released in May 2020
pymysql==0.10.1        # Released in May 2020
pytest==5.4.3          # Released in June 2020
python-libsbml==5.11.4 
scipy==1.5.0           # Released in June 2020
sqlalchemy==1.3.18     # Released in June 2020
tabulate==0.8.7        # Released in April 2020
tqdm==4.47.0           # Released in July 2020
sphinx==3.1.2          # Released in July 2020
sphinx-rtd-theme==0.5.0  # Released in July 2020
optlang==1.4.4         # to be compatible with gurobi 9.0
jupyterlab
