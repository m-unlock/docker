import yaml
import argparse
import sys
from io import StringIO
import io, os
import cwltool.main
import subprocess

def create_environment_yaml(metadata):
    channels = set(["conda-forge"])
    dependencies = set()

    for software in metadata:
        version = metadata[software]['version']
        specs = metadata[software]['specs']
        dependencies.add(specs.split("/")[-1] + "=" + version)
        if "/bioconda/" in specs:
            channels.add("bioconda")
        elif "/conda-forge/" in specs:
            channels.add("conda-forge")
        elif specs == "None":
            # Possible due to the optional query
            # Not all CWL files have conda requirements
            continue
        else:
            raise Exception("Unknown channel", specs)
    environment = {
        "name": "start",
        "channels": list(channels),
        "dependencies": list(dependencies)
    }

    return environment

def docker_build(image):
    # Fix start script
    os.system("chmod +x start.sh")
    
    if not args.force and docker_pull_check(image):
        return

    command = "docker build  --build-arg irodsUserName="+os.environ["IRODS_USER_NAME"]+" --build-arg irodsPassword=" + os.environ["IRODS_USER_PASSWORD"] +" -t " + image + " ." # -f " + dockerfile + " ."
    print(command)
    result = subprocess.check_output(command, shell=True)
    command = "docker push  " + image
    print(command)
    os.system(command)

def docker_pull_check(image):
# Check docker pull
    command = "timeout 3 docker pull " + image
    print(command)

    try:
        result = subprocess.check_output(command, shell=True)
        if "Image is up to date" in str(result):
            return True
        if "Pulling from" in str(result):
            return True
    except subprocess.CalledProcessError as result:                                                                                                   
        # print("error code", result.returncode, result.output)
        if "Pulling from" in str(result.output):
            return True
        if "Already exists" in str(result.output):
            return True
        if "not found: manifest unknown" in str(result.output):
            return False
        if "returned non-zero exit status 1." in str(result.output):
            return False
        else:
            print("### TO BE CAPTURED!!!!")
            print("> ", result)

def prepare_image(metadata, image):
    folder = image.split("/")[-1].split(":")[0]
    # Current directory
    curdir = os.getcwd()
    # Move to docker specific folder
    os.chdir("./docker/" + folder)
    # Create environment file
    environment = create_environment_yaml(metadata)
    
    environment_file = open("./environment.yml", "w")
    print(yaml.dump(environment), file=environment_file)
    environment_file.close()
    
    docker_build(image)

    print("Moving to ", curdir)
    os.chdir(curdir)



def prepare_conda_image(metadata, image):
    # print(metadata, image)
    # Only for conda tools
    environment = create_environment_yaml(metadata)
        
    environment_file = open("./docker/anaconda/environment.yml", "w")
    print(yaml.dump(environment), file=environment_file)
    environment_file.close()

    # Execute docker build from the directory
    curdir = os.getcwd()
    # Changing dir to dockerfile location
    os.chdir("./docker/anaconda/")
    docker_build(image)

    print("Moving to ", curdir)
    os.chdir(curdir)


args = None

def main():
    # Arguments
    global args
    args = args_parser()

    # docker login docker-registry.wur.nl
    # docker build -t docker-registry.wur.nl/m-unlock/docker .
    # docker push docker-registry.wur.nl/m-unlock/docker

    # Get RDF representation of workflow
    cwltool_arguments = ["--print-rdf", args.cwl]
    f = io.StringIO()
    cwltool.main.main(cwltool_arguments, stdout=f)
    out = f.getvalue()
    output_file = open("workflow.ttl", "w")
    print(out, file=output_file)
    output_file.close()

    # Load into local triple store
    import rdflib.graph as g
    graph = g.Graph()
    graph.parse('workflow.ttl', format='turtle')

    qres = graph.query("""
    PREFIX cwl: <https://w3id.org/cwl/cwl#>
    SELECT DISTINCT ?step ?package ?version ?specs ?image
    WHERE {
    OPTIONAL {
    ?step cwl:hints ?hint .
    ?hint <https://w3id.org/cwl/cwl#SoftwareRequirement/packages> ?packages .
    ?packages <https://w3id.org/cwl/cwl#SoftwarePackage/version> ?version .
    ?packages <https://w3id.org/cwl/cwl#SoftwarePackage/package> ?package .
    ?packages <https://w3id.org/cwl/cwl#SoftwarePackage/specs> ?specs .
    ?step cwl:hints ?dockerhint .
    }
    ?dockerhint a cwl:DockerRequirement .
    ?dockerhint <https://w3id.org/cwl/cwl#DockerRequirement/dockerPull> ?image .
    }""")

    packages = {}
    for result in qres:
        # print(result)
        if str(result.image) not in packages:
            packages[str(result.image)] = {}
        packages[str(result.image)][str(result.package)] = {"version": str(result.version), "specs":str(result.specs)}

    for image in packages:
        folder = image.split("/")[-1].split(":")[0]
        print("Folder: ", folder)
        
        if os.path.isdir("./docker/" + folder):
            prepare_image(packages[image], image)
        else:
            prepare_conda_image(packages[image], image)


def args_parser():
    parser = argparse.ArgumentParser(description='Arguments.....')
    parser.add_argument('--cwl', dest='cwl', help='Path to the cwl workflow file', type=str)
    parser.add_argument('--force', dest='force', action='store_true', help='Force rebuilding of docker image')
    args = parser.parse_args()
    print(args)
    return args


if __name__ == '__main__':
    if len(sys.argv) == 1:
        args = [ "--cwl", "../../../cwl/cwl/workflows/workflow_metagenomics_assembly.cwl" ]
        print("Test settings used", args)
        sys.argv[1:] = args
    main()
